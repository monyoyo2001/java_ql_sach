<%@page import="bean.dangnhapadminbean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
  <title>Book</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
	
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark justify-content-between">
	  <ul class="navbar-nav">
	    <li class="nav-item active">
	      <a class="nav-link" href="htLoaiAdminController">Quản lý loại</a>
	    </li>
	    <li class="nav-item active">
	      <a class="nav-link" href="sachAdminController">Quản lý sách</a>
	    </li>
	    <li class="nav-item active">
	      <a class="nav-link" href="xacnhanController">Xác nhận chuyển tiền</a>
	    </li>
	  </ul>
	  <div class="nav float-right " id="collapsibleNavbar">
	    <ul class="navbar-nav">
	      <li class="nav-item">
	        <%if(session.getAttribute("admin") != null){ %>
	        	<a class="nav-link" href="#">
				      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6" style="width:18xp; height: 18px;">
						  <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9" />
					</svg>
						Hi <%dangnhapadminbean dn = (dangnhapadminbean)session.getAttribute("admin");
							out.print(dn.getTenDangNhap()); %>
			      </a>
		      <%} else { %>
			      <a class="nav-link" href="dangnhapadminController">
		        	<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6" style="width:18xp; height: 18px;">
					  <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9" />
					</svg>
		        	Đăng nhập
		        </a>
			      <%} %>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="thoatController	">
	        	<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6" style="width:18xp; height: 18px;">
	 					<path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15M12 9l-3 3m0 0l3 3m-3-3h12.75" />
				</svg>
				Đăng xuất
			</a>
	      </li>
	    </ul>
	 </div>  
	</nav>
	
	<section>
		 <div class="container mt-2">
			 <div class= "row">
			 	<div class="col-4">
			 		 <a class="btn btn-outline-warning" href="loaiAdminController">Thêm</a>
			 	</div>
			 	<div class="col-8">
			 		<form class="d-flex" role="search" method="get">
				      <input class="form-control" type="search" placeholder="Tìm kiếm loại sách" aria-label="Search">
				      <button class="btn btn-outline-success mx-2" type="submit">Search</button>
				    </form>
			 	</div>
			 </div>
  		</div>
  		
  		<div class="container-sm mt-2">
  			<table class="table table-bordered">
				<thead align="center">
				    <tr>
				      <th>Mã loại</th>
				      <th>Tên loại</th>
				      <th></th>
				    </tr>
				 </thead>
				 <tbody align="center">
				 <c:forEach var = "i" items="${dsloai}">
				 	<tr >
						<td>${i.getMaloai() }</td>
						<td>${i.getTenloai() }</td>
						<td>
						<a href=""><i class="fas fa-edit px-1" style="font-size: 24px; color: #18978F;"></i></a>
						<a href="DeleteSachController?ml=${i.getMaloai()}"><i class="fas fa-trash px-1" style="font-size: 24px; color: #DD5353;"></i></a>
						</td>
					</tr>
				 </c:forEach>
					
				  </tbody>
			</table>
  		</div>
	</section>
</body>
</html>