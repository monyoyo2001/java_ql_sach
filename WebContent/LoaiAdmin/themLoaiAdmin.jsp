<%@page import="bean.dangnhapadminbean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
  <title>Book</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
	
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark justify-content-between">
	  <ul class="navbar-nav">
	    <li class="nav-item active">
	      <a class="nav-link" href="loaiAdminController">Quản lý loại</a>
	    </li>
	    <li class="nav-item active">
	      <a class="nav-link" href="sachAdminController">Quản lý sách</a>
	    </li>
	    <li class="nav-item active">
	      <a class="nav-link" href="xacnhanController">Xác nhận chuyển tiền</a>
	    </li>
	  </ul>
	  <div class="nav float-right " id="collapsibleNavbar">
	    <ul class="navbar-nav">
	      <li class="nav-item">
	        <%if(session.getAttribute("admin") != null){ %>
	        	<a class="nav-link" href="#">
				      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6" style="width:18xp; height: 18px;">
						  <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9" />
					</svg>
						Hi <%dangnhapadminbean dn = (dangnhapadminbean)session.getAttribute("admin");
							out.print(dn.getTenDangNhap()); %>
			      </a>
		      <%} else { %>
			      <a class="nav-link" href="dangnhapadminController">
		        	<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6" style="width:18xp; height: 18px;">
					  <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9" />
					</svg>
		        	Đăng nhập
		        </a>
			      <%} %>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="thoatController	">
	        	<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6" style="width:18xp; height: 18px;">
	 					<path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15M12 9l-3 3m0 0l3 3m-3-3h12.75" />
				</svg>
				Đăng xuất
			</a>
	      </li>
	    </ul>
	 </div>  
	</nav>
	
	<div  class="container mt-2">
		<form class="col-6" method="POST" action="loaiAdminController">
		  <div class="mb-3">
		    <label for="exampleInputEmail1" class="form-label">Mã loại:</label>
		    <input type="text" class="form-control" id="exampleInputEmail1" name="txtmaloai">
		  </div>
		  <div class="mb-3">
		    <label for="exampleInputPassword1" class="form-label">Tên loại:</label>
		    <input type="text" class="form-control" id="exampleInputPassword1" name="txttenloai">
		  </div>
		  <%
			if(request.getParameter("error")!=null){
				out.print("<h6 style=\"color: #D2001A;\">Vui lòng điền đầy đủ thông tin!</h6>");
			}
		  %>
		  <button type="submit" class="btn btn-success">Lưu lại</button>
		  <a href="htLoaiAdminController" class="btn btn-danger mx-3">Quay lại</a>
		</form>
	</div>
</body>
</html>