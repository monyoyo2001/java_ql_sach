<%@page import="bo.giohangbo"%>
<%@page import="bean.loaibean"%>
<%@page import="bo.loaibo"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
  <title>SignIn</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style type="text/css">
		.login-container{
		    margin-top: 2%;
		    margin-bottom: 5%;
		  	margin-right: 1%;
		}
		.login-logo{
		    position: relative;
		    margin-left: -41.5%;
		}
		.login-logo img{
		    position: absolute;
		    width: 20%;
		    margin-top: 19%;
		    background: #282726;
		    border-radius: 4.5rem;
		    padding: 5%;
		}
		.login-form-1{
		    padding: 9%;
		    background:#282726;
		    box-shadow: 0 5px 8px 0 rgba(0, 0, 0, 0.2), 0 9px 26px 0 rgba(0, 0, 0, 0.19);
		}
		.login-form-1 h3{
		    text-align: center;
		    margin-bottom:12%;
		    color:#fff;
		}
		.login-form-2{
		    padding: 9%;
		    background: #f05837;
		    box-shadow: 0 5px 8px 0 rgba(0, 0, 0, 0.2), 0 9px 26px 0 rgba(0, 0, 0, 0.19);
		}
		.login-form-2 h3{
		    text-align: center;
		    margin-bottom:12%;
		    color: #fff;
		}
		
		.btnSubmit{
		    font-weight: 600;
		    width: 50%;
		    color: #282726;
		    background-color: #fff;
		    border: none;
		    border-radius: 1.5rem;
		    padding:2%;
		}
		
		.btnForgetPwd{
		    color: #fff;
		    font-weight: 600;
		    text-decoration: none;
		}
		.btnForgetPwd:hover{
		    text-decoration:none;
		    color:#fff;
		}
  </style>
</head>
<body>
	
     
     <!-- Section: Design Block -->
	<section class="">
	  <!-- Jumbotron -->
	 <div class="px-4 py-5 px-md-5 text-center text-lg-start" style="background-color: hsl(0, 0%, 96%)">
	   <div class="container">
	     <div class="row gx-lg-5 align-items-center">
	       <div class="col-lg-6 mb-5 mb-lg-0">
	         <img src="https://i.pinimg.com/564x/a1/c6/45/a1c6456ac619d859daa9af8b23c90376.jpg"
	          alt="Login image" class="w-100 vh-100" style="object-fit: cover; object-position: left;">
	       </div>
	       
	       <div class="col-lg-6 mb-5 mb-lg-0">
	         <div class="card">
	           <div class="card-body py-5 px-md-5">
	           	<h2 class="fw-bold mb-2 text-uppercase text-center">Sign up</h2>
	           
	             <form action="dangkyController" method="get">
	               <!-- Email input -->
	               <div class="form-outline mb-4">
	                	<input type="text" class="form-control" name="nameuser" placeholder="Nhập Họ và tên"/>	
	               </div>
	               
	               <div class="form-outline mb-4">
	               	<input type="text" class="form-control" name="dcuser" placeholder="Nhập Địa chỉ" />
	               </div>
	               
	               <div class="form-outline mb-4">
	                	<input type="text" class="form-control" name="sdtuser" placeholder="Nhập Số điện thoại" />	
	               </div>
	               
	               <div class="form-outline mb-4">
	                	<input type="email" class="form-control" name="emailuser" placeholder="Nhập Email" />	
	               </div>
	               
	               <div class="form-outline mb-4">
	               	<input name="user" type="text" class="form-control" placeholder="Nhập Tên tài khoản" value="" />	
	               </div>
	               
	               <div class="form-outline mb-4">
	                 <input name="pass" type="password" class="form-control" placeholder="Nhập Password" value="" />	
	               </div>
	               
	
	               <div class="form-outline mb-4">
	                	 <input name="repass" type="password" class="form-control" placeholder="Nhập lại Password" value="" />
	               </div>
	             	<%
	            	 if(request.getParameter("ktr")!=null){
						out.print("<h6 style=\"color: #D2001A;\">Vui lòng không được để trống!</h6>");
					}
	             	if(request.getParameter("ktt")!=null){
						out.print("<h6 style=\"color: #D2001A;\">Mật khẩu nhập lại không khớp!</h6>");
					}
					if(request.getParameter("kt")!=null){
						out.print("<h6 style=\"color: #D2001A;\">Tên đăng nhập bị trùng!</h6>");
					}
					
				%>
	
	               <!-- Submit button -->
	               <button type="submit" class="btn btn-outline-success btn-rounded mb-4">
	                 Sign up
	               </button>
	
	               <!-- Register buttons -->
	               <div class="text-center">
	                 <p>or login with:</p>
	                 <a href="loginKH.jsp" class="btn btn-outline-info btn-rounded mb-4">Login</a>
	               </div>
	             </form>
	           </div>
	         </div>
	       </div>
	     </div>
	   </div>
	 </div>
	 <!-- Jumbotron -->
	</section>
	<!-- Section: Design Block -->
</body>
</html>