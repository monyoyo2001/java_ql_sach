<%@page import="bo.giohangbo"%>
<%@page import="bean.loaibean"%>
<%@page import="bo.loaibo"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark justify-content-between">
		  <ul class="navbar-nav">
		    <li class="nav-item active">
		      <a class="nav-link" href="htsachController">Trang chủ</a>
		    </li>
		    <li class="nav-item">
		      <%if(session.getAttribute("gio") != null) 
		     	{
		    	  giohangbo gh = (giohangbo)session.getAttribute("gio");
		     	
		     	%>
			      <a class="nav-link" href="htgiohangController">
			      	<i class="fas fa-cart-plus"></i>
			      	<div class="badge badge-danger"><%=gh.count()%></div>
			      </a>
		      	<%}else { %>
			      <a class="nav-link" href="htgiohangController">
			      	<i class="fas fa-cart-plus"></i>
			      	<div class="badge badge-danger">0</div>
			      </a>
		      <% } %>
		    </li>
		    <li class="nav-item">
		      <a class="nav-link" href="thanhtoan.jsp">Thanh toán</a>
		    </li>
		    <li class="nav-item">
		      <a class="nav-link" href="lichsu.jsp">Lịch sử mua hàng</a>
		    </li>
		  </ul>
		  <div class="nav float-right " id="collapsibleNavbar">
		    <ul class="navbar-nav">
		      <li class="nav-item">
		        <%if(session.getAttribute("dn") != null){ %>
		        	<a class="nav-link" href="#">
					      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6" style="width:18xp; height: 18px;">
							  <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9" />
						</svg>
							Hi <%=session.getAttribute("dn") %>
				      </a>
			      <%} else { %>
				      <a class="nav-link" href="ktdnController">
			        	<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6" style="width:18xp; height: 18px;">
						  <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9" />
						</svg>
			        	Đăng nhập
			        </a>
				      <%} %>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="thoatController	">
		        	<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6" style="width:18xp; height: 18px;">
		 					<path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15M12 9l-3 3m0 0l3 3m-3-3h12.75" />
					</svg>
					Đăng xuất
				</a>
		      </li>
		    </ul>
		 </div>  
	</nav>
	
	
	<div class="container-fluid" align="center">
		<h2>WELCOME </h2>
			<div class="col-3"> 
				<%-- <form style="margin-bottom: 10px;" action="ktdnController" method="post">
					un= <input name="txtun" type="text" placeholder="Nhập tài khoản" style="padding: 6px" > <br>
					pass=<input name="txtpass" type="text" placeholder="Nhập mật khẩu" style="padding: 6px"> <br>
					<%
						if(request.getParameter("kt")!=null){
							out.print("<p>Đăng nhập sai!</p>");
						}
					%>
					<input name="butt" type="submit" value="Login" style="padding: 6px;">
				</form>
				 --%>
				
				
				<form action="ktdnController" method="post" class="form" >
				   <div class="form-group" >
				     <label>Tên đăng nhập</label>
				     <input type="text" class="form-control" placeholder="Nhập tài khoản" name="txtun">
				   </div>
				   <div class="form-group">
				     <label >Mật khẩu</label>
				     <input type="text" class="form-control" placeholder="Nhập mật khẩu" name="txtpass">
				   </div>
				   <%
						if(request.getParameter("kt")!=null){
							out.print("<h6 style=\"color: #D2001A;\">Đăng nhập sai!</h6>");
						}
					%>
				   <button name="butt"  type="submit" class="btn btn-primary">Đăng nhập</button>
				   <a href="dangky.jsp" class="btn btn-outline-warning">Đăng ký</a>
				</form>
				
		</div>
	</div>

	
</body>
</html>