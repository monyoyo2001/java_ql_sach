<%@page import="bo.giohangbo"%>
<%@page import="bean.loaibean"%>
<%@page import="bo.loaibo"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
  <title>Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style type="text/css">
		.login-container{
		    margin-top: 2%;
		    margin-bottom: 5%;
		  	margin-right: 1%;
		}
		.login-logo{
		    position: relative;
		    margin-left: -41.5%;
		}
		.login-logo img{
		    position: absolute;
		    width: 20%;
		    margin-top: 19%;
		    background: #282726;
		    border-radius: 4.5rem;
		    padding: 5%;
		}
		.login-form-1{
		    padding: 9%;
		    background:#282726;
		    box-shadow: 0 5px 8px 0 rgba(0, 0, 0, 0.2), 0 9px 26px 0 rgba(0, 0, 0, 0.19);
		}
		.login-form-1 h3{
		    text-align: center;
		    margin-bottom:12%;
		    color:#fff;
		}
		.login-form-2{
		    padding: 9%;
		    background: #f05837;
		    box-shadow: 0 5px 8px 0 rgba(0, 0, 0, 0.2), 0 9px 26px 0 rgba(0, 0, 0, 0.19);
		}
		.login-form-2 h3{
		    text-align: center;
		    margin-bottom:12%;
		    color: #fff;
		}
		
		.btnSubmit{
		    font-weight: 600;
		    width: 50%;
		    color: #282726;
		    background-color: #fff;
		    border: none;
		    border-radius: 1.5rem;
		    padding:2%;
		}
		
		.btnForgetPwd{
		    color: #fff;
		    font-weight: 600;
		    text-decoration: none;
		}
		.btnForgetPwd:hover{
		    text-decoration:none;
		    color:#fff;
		}
  </style>
</head>
<body>
	<section class="vh-100" style="background-color: #9A616D;">
	  <div class="container py-5 h-100">
	    <div class="row d-flex justify-content-center align-items-center h-100">
	      <div class="col col-xl-10">
	        <div class="card" style="border-radius: 1rem;">
	          <div class="row g-0">
	            <div class="col-md-6 col-lg-5 d-none d-md-block">
	              <img src="https://i.pinimg.com/564x/64/0f/b0/640fb0fde5c586ce7e44d1713fde544c.jpg"
	                alt="login form" class="img-fluid" style="border-radius: 1rem 0 0 1rem;" />
	            </div>
	            <div class="col-md-6 col-lg-7 d-flex align-items-center">
	              <div class="card-body p-4 p-lg-5 text-black">
	
	                <form action="dangnhapadminController" method="post">
		                  <div class="d-flex align-items-center mb-3 pb-1">
		                    <i class="fas fa-cubes fa-2x me-3" style="color: #ff6219;"></i>
		                    <span class="h1 fw-bold mb-0">Shop</span>
		                  </div>
		
		                  <h5 class="fw-normal mb-3 pb-3" style="letter-spacing: 1px;">Sign into your account</h5>
		
		                  <div class="form-outline mb-4">
		                   	<label class="form-label" for="form2Example17">User Name:</label>
		                    <input type="text" class="form-control form-control-lg" name="txtun"/>
		                  </div>
		
		                  <div class="form-outline mb-4">
		                  	<label class="form-label" for="form2Example27">Password:</label>
		                    <input type="text" class="form-control form-control-lg" name="txtpass" />
		                  </div>
							 <%
								if(request.getParameter("kt")!=null){
									out.print("<h6 style=\"color: #D2001A;\">Đăng nhập sai!</h6>");
								}
							%>
		                  <div class="pt-1 mb-4">
		                    <button class="btn btn-dark btn-lg btn-block" type="submit">Login</button>
		                  </div>
		                  
	          				<p class="mb-5 pb-lg-2" style="color: #393f81;">
	          					Don't have an account? 
	          					<a href="dangky.jsp" style="color: #393f81;">Register here</a>
	          				</p>
          			</form>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
  		</div>
	</section>
</body>
</html>