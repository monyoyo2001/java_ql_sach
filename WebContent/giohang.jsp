<%@page import="bean.giohangbean"%>
<%@page import="bo.giohangbo"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
</head>

<body>
	<%
	String masach = request.getParameter("ms");
	String tensach = request.getParameter("ts");
	long gia = Long.parseLong(request.getParameter("gia"));
	long soluong=1;
	giohangbo gh=null;
	if(session.getAttribute("gio")==null){
		//Mua lần đầu
		gh = new giohangbo();
		session.setAttribute("gio", gh);
		
		//b1 lấy session lưu vào biến
		gh=(giohangbo)session.getAttribute("gio");
		
		//b2 thao tác trên biến
		gh.Them(masach, tensach, soluong, gia);
		
		//B3 lưu biên vào session
		session.setAttribute("gio", gh);
		
		//hiển thị giỏ hàng
		response.sendRedirect("htgio.jsp");
	}else {
		giohangbo ghang = (giohangbo)session.getAttribute("gio");
		ghang.Them(masach, tensach, soluong, gia);
		//ghang.TongTien();
		session.setAttribute("gio", ghang);
		response.sendRedirect("htgio.jsp");
	}
	%>
</body>
</html>