<%@page import="java.util.List"%>
<%@page import="bean.giohangbean"%>
<%@page import="bo.giohangbo"%>
<%@page import="bo.sachbo"%>
<%@page import="bo.loaibo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="bean.sachbean"%>
<%@page import="dao.sachdao"%>
<%@page import="bean.loaibean"%>
<%@page import="dao.loaidao"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
  <title>Cart</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark justify-content-between">
	  <ul class="navbar-nav">
	    <li class="nav-item active">
	      <a class="nav-link" href="htsachController">Trang chủ</a>
	    </li>
	    <li class="nav-item">
	     <%if(session.getAttribute("gio") != null) 
	     	{
	    	 giohangbo gh = (giohangbo)session.getAttribute("gio");
	     	
	     %>
		      <a class="nav-link" href="htgiohangController">
		      	<i class="fas fa-cart-plus"></i>
		      	<div class="badge badge-danger"><%=gh.count()%></div>
		      </a>
	      <%} else { %>
	      	 <a class="nav-link" href="htgiohangController">
		      	<i class="fas fa-cart-plus"></i>
		      	<div class="badge badge-danger">0</div>
		      </a>
	      <% }%>
	    </li>
	    <li class="nav-item">
	      <a class="nav-link" href="thanhtoan.jsp">Thanh toán</a>
	    </li>
	    <li class="nav-item">
	      <a class="nav-link" href="LichSuController">Lịch sử mua hàng</a>
	    </li>
	  </ul>
	  <div class="nav float-right " id="collapsibleNavbar">
	    <ul class="navbar-nav">
	      <li class="nav-item">
	        <%if(session.getAttribute("dangnhap") != null){ %>
	        	<a class="nav-link" href="#">
				      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6" style="width:18xp; height: 18px;">
						  <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9" />
					</svg>
						Hi <%=session.getAttribute("dangnhap") %>
			      </a>
		      <%} else { %>
			      <a class="nav-link" href="LoginKHController">
		        	<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6" style="width:18xp; height: 18px;">
					  <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9" />
					</svg>
		        	Đăng nhập
		        </a>
			      <%} %>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="thoatController">
	        	<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6" style="width:18xp; height: 18px;">
	 					<path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15M12 9l-3 3m0 0l3 3m-3-3h12.75" />
				</svg>
				Đăng xuất
			</a>
	      </li>
	    </ul>
	 </div>  
	</nav>
	
	<h1 style="margin-bottom: 10px">BÁN SÁCH ONLINE</h1>
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-2">
				<table class="table table-hover">
	      	 	  <%
	      	 		ArrayList<loaibean> dsloai =(ArrayList<loaibean>)request.getAttribute("dsloai");
	      	 	  	for(loaibean l: dsloai) {
	      	 	  %>
	      	 	     <tr>
	      	 	        <td>
	      	 	          <a href="htsachController?ml=<%=l.getMaloai()%>" style="text-decoration: none;">
	      	 	             <%=l.getTenloai() %>
	      	 	          </a>  
	      	 	        </td>
	      	 	      </tr>
	      	 	  <%} %>
      	 		</table>
			</div>
			<div class="col-7"> 
				<h2>Giỏ hàng của bạn</h1> 
				<hr>
				<%	giohangbo gh = (giohangbo)session.getAttribute("gio");
						if(gh != null){
							//giohangbo gh = (giohangbo)session.getAttribute("gio");
					%>
				<form action="SuaXoaGHController" method="post">
					<table class="table table-bordered">
						<thead align="center">
						    <tr>
						      <th></th>
						      <th>Tên sách</th>
						      <th>Giá</th>
						      <th>Số lượng</th>
						      <th></th>
						      <th>Thành tiền</th>
						    </tr>
						 </thead>
						 <tbody align="center">
						
						 		<%	for(giohangbean g: gh.ds){
									%>
									<tr >
										<td><input type="checkbox" name="check" value="<%=g.getMasach()%>"></td>
										<td><%=g.getTensach()%></td>
										<td><%=g.getGia()%></td>
										<td><%=g.getSoluong()%></td>
										<td style="display: flex; justify-content:flex-start;">
											<input name="<%=g.getMasach()%>" type="number" min="1" style="padding:3px; width: 100px; margin-right: 10px;">
											<button type="submit" name="btnsua" value="<%=g.getMasach()%>" class="btn btn-outline-info" style="margin-right: 5px;"><i class="fas fa-edit"></i></button>
											<button type="submit" name="btnxoa" value="<%=g.getMasach()%>" class="btn btn-outline-danger"><i class="far fa-trash-alt"></i></button>
										</td>
										<td><%=g.getThanhtien()%></td>
										
									</tr>
									
								<%} %>
							
						  </tbody>
						</table>
						
						
						
						<hr><h4>Tổng tiền: <%=gh.TongTien() %> VND </h4>
						<hr>
						<div align="center">
							<button type="submit" name="btnxoacheck" class="btn btn-outline-warning"><i class="fas fa-trash-alt"></i></button>
							<a href="DeleteGHController" class="btn btn-outline-danger" >Hủy hóa đơn</a>
							<a href="thanhtoanController" class="btn btn-outline-success">Xác nhận thanh toán</a>
						</div>
						
				</form>
			<%}else { %>
				<h4>Giỏ hàng đang trống !</h4>
				<a href="htsachController" class="btn btn-outline-warning" >Chọn mua hàng</a>
			<%} %>
			</div>
			<div class="col-3">
				<form style="margin-bottom: 10px;display:flex;" action="htsachController" method="post">
					<input name="txttk" type="text" value="" placeholder="Nhập sách cần tìm kiếm" style="margin-right:10px"> <br>
   					<input name="butt" type="submit" value="Search" class="btn btn-info">
				</form>
			</div>
		</div>
	</div>
</body>
</html>