<%@page import="bean.lichsumahangbean"%>
<%@page import="java.util.List"%>
<%@page import="bo.giohangbo"%>
<%@page import="bo.sachbo"%>
<%@page import="bo.loaibo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="bean.sachbean"%>
<%@page import="dao.sachdao"%>
<%@page import="bean.loaibean"%>
<%@page import="dao.loaidao"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
  <title>Checkout</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

	<nav class="navbar navbar-expand-sm bg-dark navbar-dark justify-content-between">
	  <ul class="navbar-nav">
	    <li class="nav-item active">
	      <a class="nav-link" href="htsachController">Trang chủ</a>
	    </li>
	    <li class="nav-item">
	      <%if(session.getAttribute("gio") != null) 
	     	{
	    	  giohangbo gh = (giohangbo)session.getAttribute("gio");
	     	
	     %>
		      <a class="nav-link" href="htgiohangController">
		      	<i class="fas fa-cart-plus"></i>
		      	<div class="badge badge-danger"><%=gh.count()%></div>
		      </a>
	      <%}else { %>
		      <a class="nav-link" href="htgiohangController">
		      	<i class="fas fa-cart-plus"></i>
		      	<div class="badge badge-danger">0</div>
		      </a>
	      <% } %>
	    </li>
	    <li class="nav-item">
	      <a class="nav-link" href="thanhtoan.jsp">Thanh toán</a>
	    </li>
	    <li class="nav-item">
	      <a class="nav-link" href="LichSuController">Lịch sử mua hàng</a>
	    </li>
	  </ul>
	  <div class="nav float-right " id="collapsibleNavbar">
	    <ul class="navbar-nav">
	      <li class="nav-item">
	        <%if(session.getAttribute("dangnhap") != null){ %>
	        	<a class="nav-link" href="#">
				      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6" style="width:18xp; height: 18px;">
						  <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9" />
					</svg>
						Hi <%=session.getAttribute("dangnhap") %>
			      </a>
		      <%} else { %>
			      <a class="nav-link" href="LoginKHController">
		        	<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6" style="width:18xp; height: 18px;">
					  <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9" />
					</svg>
		        	Đăng nhập
		        </a>
			      <%} %>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="thoatController	">
	        	<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6" style="width:18xp; height: 18px;">
	 					<path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15M12 9l-3 3m0 0l3 3m-3-3h12.75" />
				</svg>
				Đăng xuất
			</a>
	      </li>
	    </ul>
	 </div>  
	</nav>

	<section class="cart_area section_padding">
		<div class="container">
			<div class="cart_inner">
				<c:if test="${dslichsu==null}">
					<h2 style="text-align: center; color: red">LỊCH SỬ TRỐNG</h2>
				</c:if>
				
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th scope="col">STT</th>
									<th scope="col">Tên sách</th>
									<th scope="col">Giá</th>
									<th scope="col">Số lượng</th>
									<th scope="col">Ngày mua</th>
									<th scope="col">Thành tiền</th>
									<th scope="col">Tình trạng</th>
								</tr>
							</thead>
							<% ArrayList<lichsumahangbean> dsls =(ArrayList<lichsumahangbean>)request.getAttribute("dslichsu");
								int i = 1;
								for(lichsumahangbean ls : dsls){ %>
							<tbody>
									<tr>
										<td><%=i %></td>
										<td><%=ls.getTensach() %></td>
										<td><%=ls.getGia() %></td>
										<td><%=ls.getSoLuongMua() %></td>
										<td><%=ls.getNgayMua() %></td>
										<td><%=ls.getThanhtien() %></td>
										<td> 
											<% if(ls.getDamua() == 0) {%> 
										      	<p>Đã đặt hàng</p> 
										     <%}else { %>
										      	<p>Đã chuyển tiền</p>
										     <%} %>
											
										</td>
									</tr>
							</tbody>
							<%i++;} %>
						</table>
					</div>
				
			</div>
		</div>
	</section>


</body>
</html>