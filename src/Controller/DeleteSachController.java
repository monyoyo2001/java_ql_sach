package Controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.loaibean;
import bo.loaibo;

/**
 * Servlet implementation class DeleteSachController
 */
@WebServlet("/DeleteSachController")
public class DeleteSachController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteSachController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 try {
			 RequestDispatcher rd = null;
			 loaibo lbo = new loaibo();
			 String maLoai = request.getParameter("ml");
			 loaibean getloai = lbo.getMotLoai(maLoai);
			 
			 if(request.getParameter("ml") != null ) {
				 lbo.delete(maLoai);
				 response.sendRedirect("htLoaiAdminController");
			 }
			 
			 request.setAttribute("getLoai", getloai);
			 request.setAttribute("getLoi", new loaibo());
			 
			 rd = request.getRequestDispatcher("LoaiAdmin/XoaLoaiAdmin.jsp");
			 
			 rd.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
