package Controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.giohangbean;
import bean.khachhangbean;
import bean.lichsumahangbean;
import bo.khachhangbo;
import bo.lichsumuahangbo;

/**
 * Servlet implementation class lichsumuahangController
 */
@WebServlet("/LichSuController")
public class LichSuController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LichSuController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {

			HttpSession session = request.getSession();

			khachhangbo khbo = new khachhangbo();
			khachhangbean khbean = new khachhangbean();
			lichsumuahangbo lsmh = new lichsumuahangbo();
			RequestDispatcher rd = null ;
			//ArrayList<lichsumahangbean> ls;

			if (session.getAttribute("dangnhap") != null) {
				String taiKhoan = (String) session.getAttribute("dangnhap");
				khbean = khbo.checkTKTT(taiKhoan);
				request.setAttribute("dslichsu", lsmh.getLichSuMuaHangKH(khbean.getMakh()));
				rd = request.getRequestDispatcher("lichsu.jsp");
			} else {
				rd = request.getRequestDispatcher("LoginKHController");
			}
			
			rd.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
