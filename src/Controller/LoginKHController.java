package Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bo.khachhangbo;

/**
 * Servlet implementation class LoginKHController
 */
@WebServlet("/LoginKHController")
public class LoginKHController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginKHController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		khachhangbo kh = new khachhangbo();
		String un = request.getParameter("txtun");
		String pass = request.getParameter("txtpass");
		RequestDispatcher rd = null ;
		if(un !=null && pass !=null) {
			try {
				if(kh.dangNhap(un, pass)){
					//tạo đối tượng session
					HttpSession session = request.getSession();
					session.setAttribute("dangnhap", un);
					rd = request.getRequestDispatcher("htsachController");
				}else {
					rd = request.getRequestDispatcher("loginKH.jsp?kt=1");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}else {
			rd = request.getRequestDispatcher("loginKH.jsp");
		}
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
