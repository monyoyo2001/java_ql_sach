package Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.giohangbean;
import bean.khachhangbean;
import bo.chitiethoadonbo;
import bo.giohangbo;
import bo.hoadonbo;
import bo.khachhangbo;

/**
 * Servlet implementation class SuaXoaGHController
 */
@WebServlet("/SuaXoaGHController")
public class SuaXoaGHController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SuaXoaGHController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher rd;
		HttpSession session = request.getSession();
		giohangbo gh = (giohangbo)session.getAttribute("gio");
		if(request.getParameter("btnxoacheck")!= null){
			String [] check = request.getParameterValues("check");
			if(check != null){
				for(String c:check){
					gh.Xoa(c);
				}
			}
		}
		
		if(request.getParameter("btnsua") !=null){
			String masach = request.getParameter("btnsua");
			int soluong=Integer.parseInt(request.getParameter(masach));
			gh.Them(masach, "", soluong, 0);
		}
		
		if(request.getParameter("btnxoa")!=null){
			String ms = request.getParameter("btnxoa");
			gh.Xoa(ms);
		}
		
		
		
		session.setAttribute("gio", gh);
		if(gh.ds.size() == 0){
			rd = request.getRequestDispatcher("htsachController");
		}else {
			rd = request.getRequestDispatcher("htgiohangController");
		}
		
		rd.forward(request, response);
		
		
		
		
				
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
