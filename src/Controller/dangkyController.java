package Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.org.apache.bcel.internal.generic.DSTORE;

import bean.khachhangbean;
import bo.dangnhapbo;
import bo.khachhangbo;

/**
 * Servlet implementation class dangkyController
 */
@WebServlet("/dangkyController")
public class dangkyController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public dangkyController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			request.setCharacterEncoding("utf-8");
			response.setCharacterEncoding("utf-8");
			
			String nameuser = request.getParameter("nameuser");
			String dcuser = request.getParameter("dcuser");
			String emailuser = request.getParameter("sdtuser");
			String sdtuser = request.getParameter("emailuser");
			String user = request.getParameter("user");
			String pass = request.getParameter("pass");
			String repass = request.getParameter("repass");
			
			RequestDispatcher rd ;
			if(nameuser != null || dcuser !=null || emailuser != null || sdtuser != null || user != null || pass !=null || repass !=null) {
				if (nameuser == "" && dcuser == "" && emailuser == "" && sdtuser == "" && user == "" && pass == "") {
					response.sendRedirect("dangky.jsp?ktr=1");
				}else {
					if(!pass.equals(repass) || user == "") {
						response.sendRedirect("dangky.jsp?ktt=1");
					} else {
						khachhangbo dk = new khachhangbo();
						if(dk.checkTaiKhoan(user)) {
							response.sendRedirect("dangky.jsp?kt=1");
						} else {
							dk.dangKy(new khachhangbean(nameuser, dcuser, sdtuser, emailuser, user, repass));
							response.sendRedirect("loginKH.jsp");
						}	
					}
				}
				
			}else {
				response.sendRedirect("dangky.jsp");
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
