package Controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.loaibean;
import bo.giohangbo;
import bo.loaibo;

/**
 * Servlet implementation class giohangController
 */
@WebServlet("/giohangController")
public class giohangController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public giohangController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		giohangbo gh=null;

	
		//hiện thị giỏ hàng
		String masach = request.getParameter("ms");
		String tensach = request.getParameter("ts");
		String giasp = request.getParameter("gia");
		long gia = Long.parseLong(giasp);
		int soluong = 1;
		if(session.getAttribute("gio")==null){
			//Mua lần đầu
			gh = new giohangbo();
			session.setAttribute("gio", gh);
			
			//b1 lấy session lưu vào biến
			gh=(giohangbo)session.getAttribute("gio");
			
			//b2 thao tác trên biến
			gh.Them(masach, tensach, soluong, gia);
			
			//B3 lưu biên vào session
			session.setAttribute("gio", gh);
			
			//hiển thị giỏ hàng
			//rd = request.getRequestDispatcher("htgio.jsp");
			//response.sendRedirect("htgio.jsp");
		}else {
			giohangbo ghang = (giohangbo)session.getAttribute("gio");
			ghang.Them(masach, tensach, soluong, gia);
			//ghang.TongTien();
			session.setAttribute("gio", ghang);
			//rd = request.getRequestDispatcher("htgio.jsp");
			//response.sendRedirect("htgio.jsp");
		}
		
		response.sendRedirect("htgiohangController");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
