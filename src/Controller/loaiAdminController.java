package Controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.loaibean;
import bo.loaibo;

/**
 * Servlet implementation class loaiAdminController
 */
@WebServlet("/loaiAdminController")
public class loaiAdminController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public loaiAdminController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			RequestDispatcher rd;
			
			String maLoai = request.getParameter("txtmaloai");
			String tenLoai = request.getParameter("txttenloai");
			loaibo lbo = new loaibo();
			
			if(maLoai != null && tenLoai != null) {
				if(maLoai == "" || tenLoai == "") {
					rd = request.getRequestDispatcher("LoaiAdmin/themLoaiAdmin.jsp?error=1");
				}else {
					lbo.themLoai(new loaibean(maLoai, tenLoai));
					response.sendRedirect("htLoaiAdminController");
				}
			}
			rd = request.getRequestDispatcher("LoaiAdmin/themLoaiAdmin.jsp");
			rd.forward(request, response);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
