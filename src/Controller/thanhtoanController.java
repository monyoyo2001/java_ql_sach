package Controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.giohangbean;

import bean.khachhangbean;
import bo.chitiethoadonbo;
import bo.giohangbo;
import bo.hoadonbo;
import bo.khachhangbo;
import bo.lichsumuahangbo;


/**
 * Servlet implementation class thanhtoanController
 */
@WebServlet("/thanhtoanController")
public class thanhtoanController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public thanhtoanController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		khachhangbo khbo = new khachhangbo();
		khachhangbean khbean = new khachhangbean();
		hoadonbo hd = new hoadonbo();
		chitiethoadonbo cthd = new chitiethoadonbo();
		lichsumuahangbo lsmh = new lichsumuahangbo();
		giohangbo gh = (giohangbo)session.getAttribute("gio");

		try {
			if (session.getAttribute("dangnhap") != null) {
				String taiKhoan = (String) session.getAttribute("dangnhap");
				khbean = khbo.checkTKTT(taiKhoan);
				hd.themHD(khbean.getMakh());
				int mahd = hd.getLastId();
				System.out.println(mahd);
				for (giohangbean g : gh.ds) {
					cthd.themCTHD(g.getMasach(), g.getSoluong(), mahd);
				}
				
				//session.removeAttribute("gio");
				session.setAttribute("gio", null);
				//request.setAttribute("dslichsu", lsmh.getLichSuMuaHangKH(khbean.getMakh()));
				//response.sendRedirect("lichsu.jsp");
				response.sendRedirect("LichSuController");
			} else {
				response.sendRedirect("LoginKHController");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
