package bean;

public class chitiethoadonbean {
	private int MaChiTietHD;
	private String MaSach;
	private long SoLuongMua;
	private int MaHoaDon;
	private int damua;

	public chitiethoadonbean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public chitiethoadonbean(int maChiTietHD, String maSach, long soLuongMua, int maHoaDon, int damua) {
		super();
		MaChiTietHD = maChiTietHD;
		MaSach = maSach;
		SoLuongMua = soLuongMua;
		MaHoaDon = maHoaDon;
		this.damua = damua;
	}

	public int getMaChiTietHD() {
		return MaChiTietHD;
	}

	public void setMaChiTietHD(int maChiTietHD) {
		MaChiTietHD = maChiTietHD;
	}

	public String getMaSach() {
		return MaSach;
	}

	public void setMaSach(String maSach) {
		MaSach = maSach;
	}

	public long getSoLuongMua() {
		return SoLuongMua;
	}

	public void setSoLuongMua(long soLuongMua) {
		SoLuongMua = soLuongMua;
	}

	public int getMaHoaDon() {
		return MaHoaDon;
	}

	public void setMaHoaDon(int maHoaDon) {
		MaHoaDon = maHoaDon;
	}

	public int getDamua() {
		return damua;
	}

	public void setDamua(int damua) {
		this.damua = damua;
	}

}
