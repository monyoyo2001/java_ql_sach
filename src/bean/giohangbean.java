package bean;

public class giohangbean {
	private String masach;
	private String tensach;
	private int soluong;
	private long gia;
	private long thanhtien;

	public giohangbean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public giohangbean(String masach, String tensach, int soluong, long gia) {
		super();
		this.masach = masach;
		this.tensach = tensach;
		this.soluong = soluong;
		this.gia = gia;
	}

	public String getMasach() {
		return masach;
	}

	public void setMasach(String masach) {
		this.masach = masach;
	}

	public String getTensach() {
		return tensach;
	}

	public void setTensach(String tensach) {
		this.tensach = tensach;
	}

	public int getSoluong() {
		return soluong;
	}

	public void setSoluong(int soluong) {
		this.soluong = soluong;
	}

	public long getGia() {
		return gia;
	}

	public void setGia(long gia) {
		this.gia = gia;
	}

	public long getThanhtien() {
		return gia * soluong;
	}

	public void setThanhtien(long thanhtien) {
		this.thanhtien = thanhtien;
	}

}
