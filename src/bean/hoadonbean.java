package bean;

import java.sql.Date;

public class hoadonbean {
	private int MaHoaDon;
	private int makh;
	private Date NgayMua;
	private int damua;

	public hoadonbean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public hoadonbean(int maHoaDon, int makh, Date ngayMua, int damua) {
		super();
		MaHoaDon = maHoaDon;
		this.makh = makh;
		NgayMua = ngayMua;
		this.damua = damua;
	}

	public int getMaHoaDon() {
		return MaHoaDon;
	}

	public void setMaHoaDon(int maHoaDon) {
		MaHoaDon = maHoaDon;
	}

	public int getMakh() {
		return makh;
	}

	public void setMakh(int makh) {
		this.makh = makh;
	}

	public Date getNgayMua() {
		return NgayMua;
	}

	public void setNgayMua(Date ngayMua) {
		NgayMua = ngayMua;
	}

	public int getDamua() {
		return damua;
	}

	public void setDamua(int damua) {
		this.damua = damua;
	}

}
