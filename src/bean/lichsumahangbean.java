package bean;

import java.sql.Date;

public class lichsumahangbean {
	private int makh; 
	private String tensach;
	private long SoLuongMua;
	private long gia;
	private long thanhtien;
	private Date NgayMua;
	private int damua;

	public lichsumahangbean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public lichsumahangbean(int makh, String tensach, long soLuongMua, long gia, long thanhtien, Date ngayMua, int damua) {
		super();
		this.makh = makh;
		this.tensach = tensach;
		SoLuongMua = soLuongMua;
		this.gia = gia;
		this.thanhtien = thanhtien;
		NgayMua = ngayMua;
		this.damua = damua;
	}

	public int getMakh() {
		return makh;
	}

	public void setMakh(int makh) {
		this.makh = makh;
	}

	public String getTensach() {
		return tensach;
	}

	public void setTensach(String tensach) {
		this.tensach = tensach;
	}

	public long getSoLuongMua() {
		return SoLuongMua;
	}

	public void setSoLuongMua(long soLuongMua) {
		SoLuongMua = soLuongMua;
	}

	public long getGia() {
		return gia;
	}

	public void setGia(long gia) {
		this.gia = gia;
	}

	public long getThanhtien() {
		return thanhtien;
	}

	public void setThanhtien(long thanhtien) {
		this.thanhtien = thanhtien;
	}

	public Date getNgayMua() {
		return NgayMua;
	}

	public void setNgayMua(Date ngayMua) {
		NgayMua = ngayMua;
	}

	public int getDamua() {
		return damua;
	}

	public void setDamua(int damua) {
		this.damua = damua;
	}

}
