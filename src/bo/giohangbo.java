package bo;

import java.util.ArrayList;

import bean.giohangbean;
import bean.sachbean;

public class giohangbo {
	public ArrayList<giohangbean> ds = new ArrayList<giohangbean>();

	public void Them(String masach, String tensach, int soluong, long gia) {
		for (giohangbean s : ds) {
			if (s.getMasach().equals(masach)) {
				s.setSoluong(s.getSoluong() + soluong);
				return;
			}
		}
		ds.add(new giohangbean(masach, tensach, soluong, gia));
	}

	public long TongTien() {
		long sum = 0;
		for (giohangbean s : ds)
			sum += s.getThanhtien();
		return sum;
	}
	
//	public void Sua(String masach, long soluong) {
//		for (giohangbean s : ds) {
//			if (s.getMasach().equals(masach)) {
//				s.setSoluong(s.getSoluong() + soluong);
//				break;
//			}
//		}
//	}
	public long count() {
		long sum = 0 ;
		for(giohangbean s: ds) {
			sum += s.getSoluong();
		}
		return sum;
	}
	
	public void Xoa(String masach) {
		for (giohangbean s : ds) {
			if (s.getMasach().equals(masach)) {
				ds.remove(s);
				return;
			}
		}
	}
}
