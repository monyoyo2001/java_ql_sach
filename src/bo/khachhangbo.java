package bo;

import bean.khachhangbean;
import dao.khachhangdao;

public class khachhangbo {
	khachhangdao khDao = new khachhangdao();

	public boolean checkTaiKhoan(String taiKhoan) throws Exception {
		return khDao.checkTaiKhoan(taiKhoan);
	}

	public boolean dangKy(khachhangbean kh) throws Exception {
		return khDao.dangKy(kh);
	}
	
	public boolean dangNhap(String tendn , String pass) throws Exception {
		return khDao.dangNhap(tendn, pass);
		
	}
	
	public khachhangbean checkTKTT(String taiKhoan) throws Exception {
		return khDao.checkTKTT(taiKhoan);
	}
	
	
}
