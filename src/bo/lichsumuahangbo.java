package bo;

import java.util.ArrayList;

import bean.lichsumahangbean;
import dao.lichsumuahangdao;

public class lichsumuahangbo {
	lichsumuahangdao ls = new lichsumuahangdao();
	
	public ArrayList<lichsumahangbean> getLSMH() throws Exception{
		return ls.getLSMH();
	}
	
	public ArrayList<lichsumahangbean> getLichSuMuaHangKH(int MaKH) throws Exception {
		return ls.getLichSuMuaHangKH(MaKH);
	}
}
