package bo;

import java.util.ArrayList;

import bean.loaibean;
import dao.loaidao;

public class loaibo {
	loaidao ldao = new loaidao();
	ArrayList<loaibean> ds;
	
	public ArrayList<loaibean> getloai(){
		ds = ldao.getloai(); // sai dao lấy về
		return ds;
	}
	
	public loaibean getMotLoai(String maLoai) throws Exception { 
		return ldao.getMotLoai(maLoai);
	}
	
	public boolean themLoai(loaibean data) throws Exception {
		return ldao.themLoai(data);
	}
	
	public boolean suaLoai(loaibean data) throws Exception { 
		return ldao.suaLoai(data);
	}
	
	public boolean delete(String maLoai) throws Exception { 
		if(ldao.auditBook(maLoai)) {
			return false;
		}
		return ldao.delete(maLoai);
	}
	
	public boolean auditBook(String maLoai) throws Exception { 
		return ldao.auditBook(maLoai);
	}
}
