package bo;

import java.util.ArrayList;

import bean.sachbean;
import dao.sachdao;

public class sachbo {
	sachdao sdao = new sachdao();
	ArrayList<sachbean> ds;

	public ArrayList<sachbean> getSach() {

		ds = sdao.getSach();

		return ds;
	}

	public ArrayList<sachbean> Tim(String key) throws Exception {
		// ArrayList<sachbean> ds = getSach();
		ArrayList<sachbean> tam = new ArrayList<sachbean>();

		for (sachbean sach : ds) {
			if (sach.getTensach().toLowerCase().trim().contains(key.toLowerCase().trim())
					|| sach.getTacgia().toLowerCase().trim().contains(key.toLowerCase().trim())
					|| sach.getMaloai().toLowerCase().trim().contains(key.toLowerCase().trim()))
				tam.add(sach);
		}

		return tam;
	}

	public ArrayList<sachbean> TimMa(String ma) throws Exception {
		// ArrayList<sachbean> ds = getSach();
		ArrayList<sachbean> tam = new ArrayList<sachbean>();

		for (sachbean sach : ds) {
			if (sach.getMaloai().equals(ma))
				tam.add(sach);
		}

		return tam;
	}

}
