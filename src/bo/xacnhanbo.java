package bo;

import java.util.ArrayList;

import bean.xacnhanbean;
import dao.xacnhandao;

public class xacnhanbo {
	private xacnhandao xndao = new xacnhandao();
	
	public ArrayList<xacnhanbean> getXacNhan() throws Exception {
		return xndao.getXacNhan();
	}
	
	public boolean updateStatus(long maChiTietHD) throws Exception {
		return xndao.updateStatus(maChiTietHD);
	}
}
