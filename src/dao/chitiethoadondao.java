package dao;

import java.sql.PreparedStatement;

import bean.chitiethoadonbean;

public class chitiethoadondao {
	public void themCTHD(String maSach, long soLuongMua, int maHoaDon ) throws Exception {
			//b1 kết nối
			KetNoidao dc = new KetNoidao();
			dc.KetNoi();
			 
			//b2 câu lệnh sql
			String sql = "Insert into ChiTietHoaDon(MaSach, SoLuongMua , MaHoaDon , damua ) values (? , ? , ? , ?)";
			PreparedStatement cmd = dc.cn.prepareStatement(sql);
			cmd.setString(1, maSach);
			cmd.setLong(2, soLuongMua);
			cmd.setInt(3, maHoaDon);
			cmd.setBoolean(4, false);
			
			//b3 cập nhật
			cmd.executeUpdate();
			
			dc.cn.close();
	}	
	

}
