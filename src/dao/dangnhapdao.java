package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class dangnhapdao {
	public boolean dangNhap(String tendangnhap, String pass) throws Exception {
		boolean isValid = false;
		KetNoidao dc = new KetNoidao();
		dc.KetNoi();
		
		String sql = "select * from DangNhap where TenDangNhap = ? and MatKhau = ?";
		PreparedStatement cmd = dc.cn.prepareStatement(sql);
		cmd.setString(1, tendangnhap);
		cmd.setString(2, pass);
		ResultSet rs = cmd.executeQuery();
		
		if(rs.next()) {
			isValid = true;
		}
		
		rs.close();
		dc.cn.close();
		
		return isValid;
	}
	
}
