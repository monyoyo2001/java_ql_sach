package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

import sun.java2d.pipe.SpanShapeRenderer.Simple;

public class hoadondao {
	public void themHD(int maKH) throws Exception {
		KetNoidao dc = new KetNoidao();
		dc.KetNoi();

		String sql = "Insert into hoadon(makh, NgayMua , damua) values (? , GETDATE() ,?)";
		PreparedStatement cmd = dc.cn.prepareStatement(sql);
		cmd.setInt(1, maKH);
		// lấy ngày trong máy tính
//			Date n = new Date();
//			SimpleDateFormat dd = new SimpleDateFormat("yyyy-MM-dd");
//			String st = dd.format(n);
//			Date ngaymua = dd.parse(st);
//			cmd.setDate(2, new java.sql.Date(ngaymua.getTime()));
		cmd.setBoolean(2, false);

		cmd.executeUpdate();

		dc.cn.close();

	}

	public int getLastId() throws Exception {
		int mahd = 0;

		KetNoidao dc = new KetNoidao();
		dc.KetNoi();

		String sql = "Select Max(MaHoaDon) as max_id from hoadon";
		PreparedStatement cmd = dc.cn.prepareStatement(sql);
		ResultSet rs = cmd.executeQuery();
		if (rs.next()) {
			mahd = rs.getInt("max_id");
		}

		dc.cn.close();
		return mahd;

	}

	public static void main(String[] args) {
		Date n = new Date();
		SimpleDateFormat dd = new SimpleDateFormat("yyyy-MM-dd");
		String st = dd.format(n);
		System.out.println(st);
	}
}
