package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.khachhangbean;

public class khachhangdao {
	public boolean dangNhap(String tendn, String pass) throws Exception {
		boolean isValid = false;
		// B1: Kết nối
		KetNoidao dc = new KetNoidao();
		dc.KetNoi();

		// B2: lấy dữ liệu
		String sql = "SELECT * FROM KhachHang WHERE tendn=? and pass = ?";
		PreparedStatement cmd = dc.cn.prepareStatement(sql);
		cmd.setString(1, tendn);
		cmd.setString(2, pass);
		ResultSet rs = cmd.executeQuery();
		if (rs.next())
			isValid = true;
		dc.cn.close();
		return isValid;
	}

	public boolean checkTaiKhoan(String taiKhoan) throws Exception {
		boolean isValid = false;
		// B1: Kết nối
		KetNoidao dc = new KetNoidao();
		dc.KetNoi();

		// B2: lấy dữ liệu
		String sql = "SELECT * FROM dbo.KhachHang WHERE tendn=?";
		PreparedStatement cmd = dc.cn.prepareStatement(sql);
		cmd.setString(1, taiKhoan);
		ResultSet rs = cmd.executeQuery();
		if (rs.next())
			isValid = true;
		dc.cn.close();
		return isValid;
	}

	public boolean dangKy(khachhangbean kh) throws Exception {
		boolean isValid = false;
		// B1: ket noi
		KetNoidao dc = new KetNoidao();
		dc.KetNoi();

		// B2: lay du lieu
		String sql = "INSERT INTO dbo.KhachHang (hoten , diachi , sodt , email , tendn , pass ) VALUES(?, ?, ?, ?, ?, ?)";
		PreparedStatement cmd = dc.cn.prepareStatement(sql);
		cmd.setString(1, kh.getHoten());
		cmd.setString(2, kh.getDiachi());
		cmd.setString(3, kh.getSodt());
		cmd.setString(4, kh.getEmail());
		cmd.setString(5, kh.getTendn());
		cmd.setString(6, kh.getPass());

		if (cmd.executeUpdate() == 1)
			isValid = true;

		return isValid;
	}

	public khachhangbean checkTKTT(String taiKhoan) throws Exception {
		khachhangbean kh = new khachhangbean();
		// B1: Kết nối
		KetNoidao dc = new KetNoidao();
		dc.KetNoi();

		// B2: lấy dữ liệu
		String sql = "select makh from KhachHang where tendn = ?";
		PreparedStatement cmd = dc.cn.prepareStatement(sql);
		cmd.setString(1, taiKhoan);
		ResultSet rs = cmd.executeQuery();
		if (rs.next()) {
			kh.setMakh(rs.getInt("makh"));
		}
		dc.cn.close();
		return kh;
	}

	
}
