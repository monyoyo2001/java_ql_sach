package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.lichsumahangbean;
import bean.sachbean;

public class lichsumuahangdao {
	public ArrayList<lichsumahangbean> getLSMH() throws Exception {
		ArrayList<lichsumahangbean> ds = new ArrayList<lichsumahangbean>();

		// b1 kết nối
		KetNoidao kn = new KetNoidao();
		kn.KetNoi();

		// b2 lấy dữ liệu
		String sql = "select * from dbo.LichSuMuaHang";
		PreparedStatement cmd = kn.cn.prepareStatement(sql);
		ResultSet rs = cmd.executeQuery();

		// b3 duyệt qua tập dữ liệu trong rs và lưu vào mảng ds
		while (rs.next()) {
			int makh = rs.getInt("makh");
			String tensach = rs.getString("[Tên Sách]");
			long soLuongMua = rs.getLong("SoLuongMua");
			long gia = rs.getLong("gia");
			long thanhtien = rs.getLong("thanhtien");
			Date ngayMua = rs.getDate("NgayMua");
			int damua = rs.getInt("damua");
			ds.add(new lichsumahangbean(makh, tensach, soLuongMua, gia, thanhtien, ngayMua, damua));
		}

		// b4 đóng cổng kết nối
		rs.close();
		kn.cn.close();

		return ds;

	}

	public ArrayList<lichsumahangbean> getLichSuMuaHangKH(int MaKH) throws Exception {
		ArrayList<lichsumahangbean> ds = new ArrayList<lichsumahangbean>();

		// b1 kết nối
		KetNoidao kn = new KetNoidao();
		kn.KetNoi();

		// b2 lấy dữ liệu
		String sql = "select * from dbo.LichSu where makh = ? ";
		PreparedStatement cmd = kn.cn.prepareStatement(sql);
		cmd.setInt(1, MaKH);
		ResultSet rs = cmd.executeQuery();

		// b3 duyệt qua tập dữ liệu trong rs và lưu vào mảng ds
		while (rs.next()) {
			int makh = rs.getInt("makh");
			String tensach = rs.getString("tensach");
			long soLuongMua = rs.getLong("SoLuongMua");
			long gia = rs.getLong("gia");
			long thanhtien = rs.getLong("thanhtien");
			Date ngayMua = rs.getDate("NgayMua");
			int damua = rs.getInt("damua");
			ds.add(new lichsumahangbean(makh, tensach, soLuongMua, gia, thanhtien, ngayMua, damua));
		}

		// b4 đóng cổng kết nối
		rs.close();
		kn.cn.close();

		return ds;

	}
}
