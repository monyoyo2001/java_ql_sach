package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.jasper.tagplugins.jstl.core.ForEach;

import bean.loaibean;

public class loaidao {
	public ArrayList<loaibean> getloai(){
		try {
			ArrayList<loaibean> ds = new ArrayList<loaibean>();
			//b1 :  kết nối vào csdl
			KetNoidao kn = new KetNoidao();
			kn.KetNoi();
			
			//b2 : lấy dữ liệu
			//b2.1 thiết lập câu lệnh sql
			String sql = "select * from loai";
			PreparedStatement cmd = kn.cn.prepareStatement(sql);
			ResultSet rs = cmd.executeQuery();
			
			//b3 : duyệt qua tập dữ liệu trong rs và lưu vào mảng ds
			while(rs.next()) {
				//lấy về mã loại và tên loại
				String maloai = rs.getString("maloai"); 
				String tenloai = rs.getString("tenloai");
				ds.add(new loaibean(maloai, tenloai));
			}
			
			//b4 : đóng kết nối
			rs.close();
			kn.cn.close();
			
			return ds;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	
	public loaibean getMotLoai(String maLoai) throws Exception {
		try {
			loaibean ds = new loaibean();
			//b1 :  kết nối vào csdl
			KetNoidao kn = new KetNoidao();
			kn.KetNoi();
			
			//b2 : lấy dữ liệu
			//b2.1 thiết lập câu lệnh sql
			String sql = "Select *from loai where maloai = ?";
			PreparedStatement cmd = kn.cn.prepareStatement(sql);
			cmd.setString(1, maLoai);
			ResultSet rs = cmd.executeQuery();
			
			//b3 : duyệt qua tập dữ liệu trong rs và lưu vào mảng ds
			while(rs.next()) {
				//lấy về mã loại và tên loại
				String maloai = rs.getString("maloai"); 
				String tenloai = rs.getString("tenloai");
				ds = new loaibean(maloai, tenloai);
			}
			
			//b4 : đóng kết nối
			rs.close();
			kn.cn.close();
			
			return ds;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	
	/**
	 * thêm thể loại sach
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public boolean themLoai(loaibean data) throws Exception {
		boolean result = false;
		//b1 kết nối 
		KetNoidao kn = new KetNoidao();
		kn.KetNoi();
		
		String sql = "INSERT INTO loai (maloai, tenloai) VALUES (? , ?)";
		PreparedStatement cmd = kn.cn.prepareStatement(sql);
		cmd.setString(1, data.getMaloai());
		cmd.setString(2, data.getTenloai());
		
		if(cmd.executeUpdate() > 0) {
			result = true;
		}
		
		kn.cn.close();
		return result;
	}
	
	/**
	 *câp nhật loại dựa vào ID của thể loại đó
	 * @param data dữ liệu cần cập nhật
	 * @return
	 * @throws Exception
	 */
	public boolean suaLoai(loaibean data) throws Exception {
		boolean result = false;
		//b1 kết nối 
		KetNoidao kn = new KetNoidao();
		kn.KetNoi();
		
		String sql = "UPDATE loai SET tenloai = ? WHERE maloai = ? ";
		PreparedStatement cmd = kn.cn.prepareStatement(sql);
		cmd.setString(1, data.getTenloai());
		cmd.setString(2, data.getMaloai());
		
		
		if(cmd.executeUpdate() > 0) {
			result = true;
		}
		
		kn.cn.close();
		return result;
	}

	
	/**
	 * xóa một thể loại sach
	 * @param categoryId id truyền vào
	 * @return
	 * @throws Exception
	 */
	public boolean delete(String maLoai) throws Exception {
		boolean result = false;
		//b1 kết nối 
		KetNoidao kn = new KetNoidao();
		kn.KetNoi();
		
		String sql = "DELETE FROM loai WHERE maloai = ?";
		PreparedStatement cmd = kn.cn.prepareStatement(sql);
		cmd.setString(1, maLoai);
		
		if(cmd.executeUpdate() > 0)
			result = true;
		
		kn.cn.close();
		return result;
	}
	
	public boolean auditBook(String maLoai) throws Exception {
		boolean result = false;
		//b1 kết nối 
		KetNoidao kn = new KetNoidao();
		kn.KetNoi();
		
		String sql ="Select * from sach where maloai = ?";
		PreparedStatement cmd = kn.cn.prepareStatement(sql);
		cmd.setString(1, maLoai);

		ResultSet rs = cmd.executeQuery();
		if(rs.next()) {
			result = true;
		}
		
		kn.cn.close();
		return result;
	}
}
