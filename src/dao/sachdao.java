package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.loaibean;
import bean.sachbean;

public class sachdao {
	
	/**
	 * lấy ra tất cả loại sách
	 * @return
	 */
	public ArrayList<sachbean> getSach() {
		try {
			ArrayList<sachbean> ds = new ArrayList<sachbean>();	
			
			//b1 kết nối 
			KetNoidao kn = new KetNoidao();
			kn.KetNoi();
			
			//b2 lấy dữ liệu
			String sql = "select * from sach";
			PreparedStatement cmd = kn.cn.prepareStatement(sql);
			ResultSet rs = cmd.executeQuery();
			
			//b3 duyệt qua tập dữ liệu trong rs và lưu vào mảng ds
			while(rs.next()) {
				String masach = rs.getString("masach");
				String tensach = rs.getString("tensach"); 
				String tacgia = rs.getString("tacgia");
				Long gia = rs.getLong("gia");
				Long soluong = rs.getLong("soluong"); 
				String anh = rs.getString("anh");
				String maloai = rs.getString("maloai");
				
				ds.add(new sachbean(masach, tensach, tacgia, gia, soluong, anh, maloai));
			}
			
			//b4 đóng cổng kết nối
			rs.close();
			kn.cn.close();
			
			return ds;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
}
