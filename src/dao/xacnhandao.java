package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.lichsumahangbean;
import bean.xacnhanbean;

public class xacnhandao {
	public ArrayList<xacnhanbean> getXacNhan() throws Exception {
		ArrayList<xacnhanbean> ds = new ArrayList<xacnhanbean>();

		// b1 kết nối
		KetNoidao kn = new KetNoidao();
		kn.KetNoi();

		// b2 lấy dữ liệu
		String sql = "select * from dbo.Vxacnhan";
		PreparedStatement cmd = kn.cn.prepareStatement(sql);
		ResultSet rs = cmd.executeQuery();

		// b3 duyệt qua tập dữ liệu trong rs và lưu vào mảng ds
		while (rs.next()) {
			long MaChiTietHD = rs.getLong("MaChiTietHD");
			long MaHoaDon = rs.getLong("MaHoaDon");
			String hoten = rs.getString("hoten");
			long SoLuongMua = rs.getLong("SoLuongMua");
			long gia = rs.getLong("gia");
			long thanhtien = rs.getLong("thanhtien");
			boolean damua = rs.getBoolean("damua"); 
			ds.add(new xacnhanbean(MaChiTietHD, MaHoaDon, hoten, SoLuongMua, gia, thanhtien, damua));
		}

		// b4 đóng cổng kết nối
		rs.close();
		kn.cn.close();

		return ds;

	}
	
	/**
	 * xác nhận đơn hàng đã đc chuyển đi
	 * @param maChiTietHD
	 * @param daMua
	 * @return
	 * @throws Exception
	 */
	public boolean updateStatus(long maChiTietHD) throws Exception {
		boolean kt = false;
		
		// b1 kết nối
		KetNoidao kn = new KetNoidao();
		kn.KetNoi();
		
		// b2 lấy dữ liệu
		String sql = "UPDATE ChiTietHoaDon SET damua = ? WHERE MaChiTietHD = ?";
		PreparedStatement cmd = kn.cn.prepareStatement(sql);
		cmd.setBoolean(1, true);
		cmd.setLong(2, maChiTietHD);
		if(cmd.executeUpdate() > 0) {
			kt = true;
		}
		
		kn.cn.close();
		return kt;
	}
}
